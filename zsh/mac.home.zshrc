# pyenv
eval "$(pyenv init --path)"
# pyenv-virtualenv
# eval "$(pyenv virtualenv-init -)"

# Python virtual environment hint
function virtual_env_prompt () {
    # REPLY=${VIRTUAL_ENV+(${VIRTUAL_ENV:t}) }
    REPLY=${VIRTUAL_ENV+(${VIRTUAL_ENV:h:t}/${VIRTUAL_ENV:t}) }
}

grml_theme_add_token virtual-env -f virtual_env_prompt '%F{magenta}' '%f'
zstyle ':prompt:grml:left:setup' items rc change-root user at host path vcs newline virtual-env percent

# openblas
export LDFLAGS="-L/usr/local/opt/openblas/lib"
export CPPFLAGS="-I/usr/local/opt/openblas/include"
export PKG_CONFIG_PATH="/usr/local/opt/openblas/lib/pkgconfig"
export BLAS_H="/usr/local/opt/openblas/include/cblas.h"
export LAPACK_H="/usr/local/opt/openblas/include/lapacke.h"

# inetutils
export PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"

# Pipx Config
export PIPX_HOME="$HOME/.pipx"
export PIPX_BIN_DIR="$PIPX_HOME/bin"
export PATH="$PIPX_BIN_DIR:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

# openssl
export PATH="/usr/local/opt/openssl@3/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl@3/lib"
export CPPFLAGS="-I/usr/local/opt/openssl@3/include"
export PKG_CONFIG_PATH="/usr/local/opt/openssl@3/lib/pkgconfig"
export PYCURL_SSL_LIBRARY=openssl

# terraform
# autoload -U +X bashcompinit && bashcompinit
# complete -o nospace -C /usr/local/bin/terraform terraform
