# Intall zinit

ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname ${ZINIT_HOME})"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "${ZINIT_HOME}"

source "${ZINIT_HOME}/zinit.zsh"


# Setup compinit for zinit

autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit


# Create cache and completions dir and add to $fpath (for oh-my-zsh plugins)

mkdir -p "$ZSH_CACHE_DIR/completions"
(( ${fpath[(Ie)"$ZSH_CACHE_DIR/completions"]} )) || fpath=("$ZSH_CACHE_DIR/completions" $fpath)


# Options

setopt interactivecomments  # recognize comments


# Key binding

bindkey ' ' magic-space
bindkey "^r" history-incremental-pattern-search-backward
bindkey "^s" history-incremental-pattern-search-forward


# Load oh-my-zsh stuff

zinit for \
    OMZL::history.zsh

zinit wait lucid for \
    OMZL::clipboard.zsh \
    OMZL::directories.zsh \
    OMZL::git.zsh \
    OMZL::grep.zsh \
    OMZL::theme-and-appearance.zsh \
    OMZL::completion.zsh

zinit wait lucid for \
    OMZP::cp \
    OMZP::git \
    OMZP::pip \
    OMZP::brew 

zinit wait lucid as"completion" for \
    OMZP::pip/_pip \
    OMZP::docker-compose/_docker-compose 


# Load other plugins

# Remap Ctrl+r for Alt+r for zdharma-continuum/history-search-multi-word
zinit wait lucid for \
    blockf atpull"zinit creinstall -q ." \
        zsh-users/zsh-completions \
    bindmap'^R -> ^[r' \
        zdharma-continuum/history-search-multi-word \
    paulirish/git-open

# 用這個 completion 才能補全 image 或 container 名稱，用 Oh-My-Zsh Plugin 所產的沒辦法
zinit wait lucid as"completion" for \
    https://github.com/docker/cli/blob/master/contrib/completion/zsh/_docker

zinit wait lucid for \
    id-as'poetry-completion' \
    as'completion' \
    atclone'poetry completions zsh > _poetry' \
    atpull'%atclone' \
    blockf \
    has'poetry' \
    nocompile \
        zdharma-continuum/null


# Load autosuggestions (這個必須要是最後一個載入的 plugin 才不會造成 tab 按下後自動選取 autosuggestions 的內容)
# Fast syntax highlighting 也需要在最後才會正常顯示顏色

zinit wait lucid for \
    atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay" \
        zdharma-continuum/fast-syntax-highlighting \
    atload"!_zsh_autosuggest_start" \
        zsh-users/zsh-autosuggestions


# Load themes

zinit ice \
    as"command" \
    from"gh-r" \
    atclone"./starship init zsh > init.zsh; ./starship completions zsh > _starship" \
    atpull"%atclone" \
    src"init.zsh"

export VIRTUAL_ENV_DISABLE_PROMPT=true  # Starship will control venv prompt.
export STARSHIP_CONFIG=${HOME}/.dotfiles/zsh/theme/starship.toml
zinit light starship/starship
# pyenv

export PATH="$HOME/.pyenv/bin:$PATH"
if command -v pyenv > /dev/null; then
    # 不使用 Oh-My-Zsh 的 pyenv plugin 的原因是因為我們使用非同步的方法 init pyenv 但他們呼叫 init 時會重新設定 PATH
    # 造成 poetry shell 無法正常使用，所以我們自己 init 並透過 `--no-push-path` 讓 PATH 不進行重新設定

    # Add pyenv shims to $PATH if not already added
    if [[ -z "${path[(Re)$(pyenv root)/shims]}" ]]; then
        eval "$(pyenv init --path)"
    fi

    # Load pyenv
    eval "$(pyenv init - --no-rehash --no-push-path zsh)"

    # If pyenv-virtualenv exists, load it
    if [[ "$ZSH_PYENV_VIRTUALENV" != false && "$(pyenv commands)" =~ "virtualenv-init" ]]; then
        eval "$(pyenv virtualenv-init - zsh)"
    fi
fi

# pyenv-virtualenv
# eval "$(pyenv virtualenv-init -)"

# Python virtual environment hint
function virtual_env_prompt () {
    # REPLY=${VIRTUAL_ENV+(${VIRTUAL_ENV:t}) }
    REPLY=${VIRTUAL_ENV+(${VIRTUAL_ENV:h:t}/${VIRTUAL_ENV:t}) }
}

# grml_theme_add_token virtual-env -f virtual_env_prompt '%F{magenta}' '%f'
# zstyle ':prompt:grml:left:setup' items rc change-root user at host path vcs newline virtual-env percent

# openblas
export LDFLAGS="-L/usr/local/opt/openblas/lib $LDFLAGS"
export CPPFLAGS="-I/usr/local/opt/openblas/include $CPPFLAGS"
export PKG_CONFIG_PATH="/usr/local/opt/openblas/lib/pkgconfig"
export BLAS_H="/usr/local/opt/openblas/include/cblas.h"
export LAPACK_H="/usr/local/opt/openblas/include/lapacke.h"

# inetutils
export PATH="/usr/local/opt/inetutils/libexec/gnubin:$PATH"

# Pipx Config
export PIPX_HOME="$HOME/.pipx"
export PIPX_BIN_DIR="$PIPX_HOME/bin"
export PATH="$PIPX_BIN_DIR:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH="$HOME/.local/bin:$PATH"

# openssl
export PATH="/usr/local/opt/openssl@3/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl@3/lib $LDFLAGS"
export CPPFLAGS="-I/usr/local/opt/openssl@3/include $CPPFLAGS"
export PKG_CONFIG_PATH="/usr/local/opt/openssl@3/lib/pkgconfig"
export PYCURL_SSL_LIBRARY=openssl

# terraform
# autoload -U +X bashcompinit && bashcompinit
# complete -o nospace -C /usr/local/bin/terraform terraform

# readline
export LDFLAGS="-L/opt/homebrew/opt/readline/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/readline/include $CPPFLAGS"

# sqlite
export PATH="/opt/homebrew/opt/sqlite/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/sqlite/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/sqlite/include $CPPFLAGS"

# zlib
export LDFLAGS="-L/opt/homebrew/opt/zlib/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/zlib/include $CPPFLAGS"

# tcl-tk
export PATH="/opt/homebrew/opt/tcl-tk/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/tcl-tk/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/tcl-tk/include $CPPFLAGS"

# libpg - postgres
export PATH="/opt/homebrew/opt/libpq/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/libpq/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/libpq/include $CPPFLAGS"
export PKG_CONFIG_PATH="/opt/homebrew/opt/libpq/lib/pkgconfig $PKG_CONFIG_PATH"

# RUST
source "$HOME/.cargo/env"

# Multiple Homebrews on Apple Silicon
if [ "$(arch)" = "arm64" ]; then
    eval "$(/opt/homebrew/bin/brew shellenv)"
else
    eval "$(/usr/local/bin/brew shellenv)"
    export LDFLAGS="-L/usr/local/opt/zlib/lib"
    export CPPFLAGS="-I/usr/local/opt/zlib/include"
fi
export PATH="/usr/local/opt/mysql-client/bin:$PATH"
