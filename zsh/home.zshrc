export PATH="$HOME/.yarn/bin:$PATH"

# GOLANG
export GOPATH=/home/super/go
export GOBIN=/home/super/go/bin
export GO111MODULE=on
export PATH=$PATH:$GOPATH/bin 

# PYENV
if [ -d "$HOME/.pyenv" ]; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
fi

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

# ZSH
export TERM=xterm-256color
setopt NO_NOMATCH
