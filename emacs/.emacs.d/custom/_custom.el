;; BASIC CUSTOMIZATION
;; --------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values (quote ((external-format . utf-8)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; 設定文字
;; (set-frame-font "-WenQ-WenQuanYi Zen Hei Mono-normal-normal-normal-*-30-*-*-*-*-0-iso10646-1")

;; 開始就最大化
(toggle-frame-fullscreen)

;; hide the startup message
(setq inhibit-startup-message t)
;; load material theme
(load-theme 'material t)
;;設定行號於左側
(global-linum-mode 1)
;; Move backup file to one path
(setq backup-directory-alist '(("" . "~/.emacs.d/.backup")))

;; Mac things
(when (string= system-type "darwin")
  (setq dired-use-ls-dired nil))
