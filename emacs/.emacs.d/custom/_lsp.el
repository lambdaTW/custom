;; performance tunning
(setq lsp-enable-file-watchers t)
(setq lsp-file-watch-threshold 3000)
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 3 1024 1024)) ;; 3mb
(setq lsp-idle-delay 0.500)
(setq lsp-log-io nil) ; if set to true can cause a performance hit

(use-package lsp-mode
  :commands (lsp lsp-deferred))

;; optional - provides fancier overlays
(use-package lsp-ui
  :commands lsp-ui-mode)

(setq lsp-enable-snippet nil)
;; (setq lsp-ui-flycheck-enable t)
(setq lsp-diagnostic-package :none)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)

;; sudo npm install -g vscode-css-languageserver-bin
;; sudo npm install -g vscode-html-languageserver-bin
(add-hook 'css-mode-hook #'lsp-deferred)
(add-hook 'html-mode-hook #'lsp-deferred)
