;;設定到文件最上方和最下方的ＫＥＹ
;(global-set-key (kbd "<C-S-iso-lefttab>") 'beginning-of-buffer)
;(global-set-key (kbd "<C-tab>") 'end-of-buffer)


;(global-set-key (kbd "<M-up>") 'beginning-of-buffer)
;(global-set-key (kbd "<M-down>") 'end-of-buffer)

;; magit key set
(global-set-key (kbd "C-x G") 'magit-status)
;; iedit key set
(define-key global-map (kbd "C-c o") 'iedit-mode)

;; buffer move
(global-set-key (kbd "s-<left>")  'windmove-left)
(global-set-key (kbd "s-<right>") 'windmove-right)
(global-set-key (kbd "s-<up>")    'windmove-up)
(global-set-key (kbd "s-<down>")  'windmove-down)
(defun prev-window ()
  (interactive)
  (other-window -1))

(define-key global-map (kbd "C-x p") 'prev-window)

;; split buttom window
(global-set-key (kbd "<f5>") (fset 'work-window
   "\C-x2\C-xo\C-x2\C-x0\C-xo")
)

