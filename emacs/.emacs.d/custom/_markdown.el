(defun my-markdown-hook ()
  (interactive)
    (define-key markdown-mode-map (kbd "M-n") 'mc/mark-next-like-this)
    (define-key markdown-mode-map (kbd "M-p") 'mc/mark-previous-like-this)
    (define-key markdown-mode-map (kbd "M-N") 'mc/mark-next-like-this)
    (define-key markdown-mode-map (kbd "M-P") 'mc/mark-previous-like-this))

(add-hook 'markdown-mode-hook 'my-markdown-hook)
