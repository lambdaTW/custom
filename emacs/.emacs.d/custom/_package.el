;;; package --- Summary
;;; Commentary:
;;; Code:
(require 'package)

(add-to-list 'package-archives
       '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-packages
  '(
    ;; --- For Emacs ---
    ;; Some defaults
    better-defaults
    ;; The material theme
    material-theme
    ;; Let Emacs show popup. The API for company packages
    popup
    ;; For apply any package with config
    use-package
    ;; Let emacs support editor config file (.editorconfig)
    editorconfig
    ;; --- For edit ---
    ;; Edit same word in same time with select word and type C-o
    iedit
    ;; Edit multiple same word with selected
    multiple-cursors
    ;; --- All language ---
    ;; Error check
    flycheck
    ;; Auto complete mode for any language
    auto-complete
    ;; Language server mode
    lsp-mode
    ;; UI integrations for lsp-mode
    lsp-ui
    ;; Company completion
    company
    ;; Git
    magit
    ;; --- Python ---
    ;; ipython
    ein
    ;; Emacs Python Development Environment.
    ;; For type C-c C-c to send script into Python interpreter
    elpy
    ;; PEP8 support
    py-autopep8
    ;; Python language server
    jedi
    ;; Language server
    lsp-pyright
    ;; --- Ruby ---
    ;; Enhance ruby mode
    ;; To highlight and indent the ruby source code
    enh-ruby-mode
    ;; inf-ruby provides a REPL buffer connected to a Ruby subprocess.
    inf-ruby
    ;; Golang mode
    ;; --- Golang ---
    go-mode
    ;; The auto complete package for Golang
    go-autocomplete
    ;; --- YAML ---
    ;; The simple YAML mode
    yaml-mode
    ;; --- Type Script ---
    ;; The simple TypeScript mode
    typescript-mode
    ;; TypeScript IDE
    tide
    ;; --- Terraform ---
    ;; The Terraform mode
    terraform-mode
    ))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      my-packages)
;;; _package.el ends here
