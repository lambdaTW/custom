;; add path to list

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'load-path "~/.emacs.d/custom")
;; require .el
(require 'highlight-indentation)

;; LOAD file
(load "_package.el")
(load "_lsp.el")
(load "_custom.el")
(load "_gkey.el")
(load "_autopair.el")
(load "_multiple-cursors.el")
(load "_highlight-indentation.el")
;; (load "_lisp.el")
(load "_python.el")
(load "_ruby.el")
(load "_go.el")
(load "_typescript.el")
(load "_company.el")
(load "_editor.el")

(setq auto-save-default nil)
(setq-default indent-tabs-mode nil)

;; flycheck
(global-flycheck-mode)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (editorconfig yaml-mode use-package typescript-mode py-autopep8 nose multiple-cursors material-theme magit lsp-ui jedi inf-ruby iedit go-mode go-autocomplete fuzzy flymake-go flycheck find-file-in-project enh-ruby-mode elpy ein dirtree company-lsp better-defaults autopair ansible-vault)))
 '(safe-local-variable-values (quote ((external-format . utf-8)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
